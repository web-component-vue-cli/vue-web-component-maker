# vue-web-component-create-tutorial

## Project setup
```
vue create project-name
```

## Creating web component
```
make component normally with .vue extension inside component folder

open terminal and run following command-
vue build --target wc --name hello-world ./src/components/HelloWorld.vue

After that you can see dist folder 

Inside dist folder you can see hello-world.js

Yes you are right hello-world.js is  a web component
```

## For implementation of web component in vue cli
```
 Please refer to readme file from  vue-web-taker project 
```
